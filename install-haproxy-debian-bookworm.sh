apt-get update -y
apt-get install -y sudo
sudo apt-get install -y curl git lua5.3 liblua5.3-dev argon2 libargon2-dev luarocks nginx gnupg
curl https://haproxy.debian.net/bernat.debian.org.gpg \
      | gpg --dearmor > /usr/share/keyrings/haproxy.debian.net.gpg
echo deb "[signed-by=/usr/share/keyrings/haproxy.debian.net.gpg]" \
      http://haproxy.debian.net bookworm-backports-2.8 main \
      > /etc/apt/sources.list.d/haproxy.list
